<?php
/**
 * Astra Addon Customizer
 *
 * @package Astra Addon
 * @since 2.0.0
 */

// Bail early if Nginx Helper is not isntalled.
if ( ! defined( 'rtCamp\WP\Nginx\RT_WP_NGINX_HELPER_PATH' ) ) {
	return;
}

/**
 * Astra Addon Page Builder Compatibility base class
 *
 * @since 2.0.0
 */
class Astra_Addon_Nginx_Helper_Compatibility {

	/**
	 * Setup the class
	 */
	public function __construct() {
		add_action( 'astra_addon_assets_refreshed', array( $this, 'refresh_nginx_helper_cache' ) );
	}

	/**
	 * Purge Nginx Helper's Cache.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function refresh_nginx_helper_cache() {
		// Nginx FastCGI using Nginx Helper.
		do_action( 'rt_nginx_helper_purge_all' );
	}

}


new Astra_Addon_Nginx_Helper_Compatibility();

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'floridanews' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uQ6l0wxBow#kswxoYR~cqO_jxU1MZje_0y(~jI$ul@h3N+G/{@[_<o{aAiZ17CBu' );
define( 'SECURE_AUTH_KEY',  '<b7$pEG^1!,xE-hB--bi*9W4,Rc(uX2`&3e]ag^H/XJE/V_So;wB73??YN]Q-]%~' );
define( 'LOGGED_IN_KEY',    'L3,kAz#Z*Ko4O3%~Tr07gF)w;w}=HqRnnBgxYaH,,N[72n,Oks_7CfQq}&L3#7[`' );
define( 'NONCE_KEY',        ':ZE|KC}IPo.teQS}Ryex62,5+Ew$Y()N~~Q+1%}A;Ix`.,i)lQ1E#~cc4]I0:4K:' );
define( 'AUTH_SALT',        '.Ab$+dPuSs(wIKA?!%*:%wbS:ZbCC?L3CD@zAga2|_([+u3Nw9){M-+I6%n08J@Z' );
define( 'SECURE_AUTH_SALT', 'emCM,=.-+(T k@(chfZeAx|G1-/=4^%7lA02Q4n(J4/-A9SQ=aR^rX4s+* pM.AE' );
define( 'LOGGED_IN_SALT',   'zGXg0yE.ZmqrmMf=&(Pl~5x48hRkCAP#JNsdx=Y>khndC-LxdVU8DX_Bj2lbg<nH' );
define( 'NONCE_SALT',       '`Ce8L9aE$AupKR&+]8Hj=}*d:qc1E<B2Q3TL^ n$=Jej>.3n)bMJ(A%GYY}Ko^h|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
